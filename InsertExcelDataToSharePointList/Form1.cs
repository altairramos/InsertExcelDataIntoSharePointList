﻿using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace InsertExcelDataToSharePointList
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        #region Proprierties

        private bool c_blnHasUserError = false;
        public bool hasUserError
        {
            get
            {
                return this.c_blnHasUserError;
            }

            set
            {
                this.c_blnHasUserError = value;
            }
        }

        #endregion

        #region Form

        public Form1()
        {
            InitializeComponent();

            LoadData();
        }

        #endregion

        #region LoadData

        public void LoadData()
        {
            txtSiteURL.Text = Properties.Settings.Default.SiteURL;
            txtUsername.Text = Properties.Settings.Default.Username;
            txtPassword.Text = Properties.Settings.Default.Password;
            txtDomain.Text = Properties.Settings.Default.Domain;
            txtSharePointList.Text = Properties.Settings.Default.SharePointList;
        }

        #endregion

        #region LoadExcelData

        public void LoadExcelData(string p_strFileName)
        {
            string strFileExtension = Path.GetExtension(p_strFileName).ToUpper();
            string strConnectionString = string.Empty;

            if (strFileExtension == ".XLS")
            {
                strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + p_strFileName + "'; Extended Properties='Excel 8.0;HDR=YES;'";
            }
            else if (strFileExtension == ".XLSX")
            {
                strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + p_strFileName + "';Extended Properties='Excel 12.0 Xml;HDR=YES;'";
            }

            if (!(string.IsNullOrEmpty(strConnectionString)))
            {
                string[] arrSheetNames = GetExcelSheetNames(strConnectionString);

                if ((arrSheetNames != null) && (arrSheetNames.Length > 0))
                {
                    DataTable dtExcelData = null;
                    OleDbConnection objOleDbConnection = new OleDbConnection(strConnectionString);
                    OleDbDataAdapter objOleDbDataAdapter = new OleDbDataAdapter("SELECT * FROM [" + arrSheetNames[0] + "]", objOleDbConnection);

                    dtExcelData = new DataTable();
                    objOleDbDataAdapter.Fill(dtExcelData);

                    InsertIntoList(dtExcelData, txtSharePointList.Text);
                }
            }
        }

        #endregion

        #region GetExcelSheetNames

        private string[] GetExcelSheetNames(string p_strConnectionString)
        {
            var objConnectionString = p_strConnectionString;
            String[] arrExcelSheets;

            using (var objConnection = new OleDbConnection(objConnectionString))
            {
                objConnection.Open();
                var objDataTable = objConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (objDataTable == null)
                {
                    return null;
                }

                arrExcelSheets = new String[objDataTable.Rows.Count];
                int intCount = 0;

                // Add the sheet name to the string array
                foreach (DataRow drRows in objDataTable.Rows)
                {
                    arrExcelSheets[intCount] = drRows["TABLE_NAME"].ToString();
                    intCount++;
                }
            }

            return arrExcelSheets;
        }

        #endregion

        #region InsertIntoList

        private void InsertIntoList(DataTable p_dtExcelData, string p_strListName)
        {
            try
            {
                string strWebURL = txtSiteURL.Text;
                string strUser = txtUsername.Text;
                string strPassword = txtPassword.Text;
                string strDomain = txtDomain.Text;

                using (ClientContext objClientContext = new ClientContext(strWebURL))
                {
                    objClientContext.Credentials = new NetworkCredential(strUser, strPassword, strDomain);

                    List objList = objClientContext.Web.Lists.GetByTitle(p_strListName);

                    for (int intRow = 0; intRow < p_dtExcelData.Rows.Count; intRow++)
                    {
                        ListItemCreationInformation objListItemCreationInformation = new ListItemCreationInformation();

                        ListItem objListItem = objList.AddItem(objListItemCreationInformation);

                        int intIndex = 0;

                        foreach (object objItemChecked in lsbSelectedColumns.Items)
                        {
                            string[] arrNameField = objItemChecked.ToString().Split(new char[] { ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            string strFieldName = arrNameField[0].Trim();
                            string strFieldType = arrNameField[1].Trim();


                            if (strFieldType.Equals("Text") || strFieldType.Equals("Note"))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex])))
                                {
                                    objListItem[strFieldName] = Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]);
                                }
                            }
                            else if (strFieldType.Equals("Choice"))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex])))
                                {
                                    objListItem[strFieldName] = Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]);
                                }
                            }
                            else if (strFieldType.Equals("DateTime"))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex])))
                                {
                                    objListItem[strFieldName] = SPUtility.CreateISO8601DateTimeFromSystemDateTime(Convert.ToDateTime(p_dtExcelData.Rows[intRow][intIndex]).AddDays(1));
                                }
                            }
                            else if (strFieldType.Equals("User"))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex])))
                                {
                                    
                                    string strUserNames = string.Empty;

                                    if(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]).Substring(Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]).Length - 1, 1).Equals(";"))
                                    {
                                        strUserNames = Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]).Substring(0, Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]).Length - 1);
                                    }
                                    else
                                    {
                                        strUserNames = Convert.ToString(p_dtExcelData.Rows[intRow][intIndex]);
                                    }

                                    string[] arrUserName = strUserNames.Split(new char[] { ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                                    FieldUserValue[] arrFieldUserValue = new FieldUserValue[arrUserName.Length];

                                    int intUserCount = 0;

                                    bool blnHasUserError = false;

                                    foreach (string strUserName in arrUserName)
                                    {
                                        try
                                        {
                                            User objUser = GetUser(strWebURL, strUser, strPassword, strDomain, strUserName.Replace(System.Environment.NewLine, ""));

                                            arrFieldUserValue[intUserCount] = new FieldUserValue();
                                            arrFieldUserValue[intUserCount].LookupId = objUser.Id;

                                            intUserCount++;
                                        }
                                        catch
                                        {
                                            Log("Error on processing user: " + strUserName.Replace(System.Environment.NewLine, "") + " - Line: " + (intRow + 1));

                                            blnHasUserError = true;
                                            continue;
                                        }
                                    }

                                    if (!blnHasUserError)
                                    {
                                        objListItem[strFieldName] = arrFieldUserValue;

                                    }
                                    else
                                    {
                                        hasUserError = true;
                                    }
                                }
                            }

                            objListItem.Update();

                            intIndex++;
                        }

                        objClientContext.ExecuteQuery();
                    }
                }
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        #endregion

        #region GetUser

        public User GetUser(string p_strWebURL, string p_strUser, string p_strPassword, string p_strDomain, string strUserName)
        {
            User objUser = null;

            try
            {
                using (ClientContext objClientContext = new ClientContext(p_strWebURL))
                {
                    objClientContext.Credentials = new NetworkCredential(p_strUser, p_strPassword, p_strDomain);

                    objUser = objClientContext.Web.EnsureUser(strUserName.Replace(System.Environment.NewLine, ""));
                    objClientContext.Load(objUser);
                    objClientContext.ExecuteQuery();
                }
            }
            catch(Exception objException)
            {
                throw objException;
            }

            return objUser;
        }

        #endregion

        #region Log
		
        public static void Log(string p_strLogMessage)
        {
            using (StreamWriter w = System.IO.File.AppendText("log.txt"))
            {
                w.Write("\r\nLog Entry: ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                w.WriteLine("  :");
                w.WriteLine("  :{0}", p_strLogMessage);
                w.WriteLine("-------------------------------");
            }
        }

        #endregion

        #region ofdExcel_FileOk

        private void ofdExcel_FileOk(object sender, CancelEventArgs e)
        {

        }

        #endregion

        #region btnOpenDialog_Click

        private void btnOpenDialog_Click(object sender, EventArgs e)
        {
            try
            {
                if (lsbSelectedColumns.Items.Count == 0)
                {
                    throw new Exception("Please, select List Columns!");
                }

                this.Enabled = false;

                ofdExcel.Filter = "Excel Worksheets | *.xlsx;*.xls";
                if (ofdExcel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtFilePath.Text = ofdExcel.FileName;
                    LoadExcelData(ofdExcel.FileName);
                }

                this.Enabled = true;

                string strMessage = "Your excel file as been processed without errors!";
                string strTitle = "Successfully";
                MessageBoxIcon objMessageBoxIcon = MessageBoxIcon.Information;

                if (hasUserError)
                {
                    strMessage = "Your excel file as been processed with errors, please see the log file!";
                    strTitle = "Error";
                    objMessageBoxIcon = MessageBoxIcon.Error;
                }

                // Clear Fields
                txtFilePath.Text = "";
                clbListColumns.Items.Clear();
                lsbSelectedColumns.Items.Clear();

                MessageBox.Show(strMessage, strTitle, MessageBoxButtons.OK, objMessageBoxIcon);
            }
            catch (Exception objException)
            {
                Log("Error on processing: " + objException.Message);
                MessageBox.Show(objException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region btnClearFilePath_Click

        private void btnClearFilePath_Click(object sender, EventArgs e)
        {
            txtFilePath.Text = "";
        }

        #endregion

        #region btnFillColumns_Click

        private void btnFillColumns_Click(object sender, EventArgs e)
        {
            try
            {
                string strWebURL = txtSiteURL.Text;
                string strUser = txtUsername.Text;
                string strPassword = txtPassword.Text;
                string strDomain = txtDomain.Text;

                clbListColumns.Items.Clear();
                lsbSelectedColumns.Items.Clear();

                using (ClientContext objClientContext = new ClientContext(strWebURL))
                {
                    objClientContext.Credentials = new NetworkCredential(strUser, strPassword, strDomain);

                    List objList = objClientContext.Web.Lists.GetByTitle(txtSharePointList.Text.Trim());

                    FieldCollection objFieldCollection = objList.Fields;
                    objClientContext.Load(objFieldCollection);
                    objClientContext.ExecuteQuery();

                    foreach (Field objField in objFieldCollection)
                    {
                        if (objField.InternalName.StartsWith("_") || objField.InternalName.StartsWith("ows") || objField.Hidden.Equals(true))
                        {
                            continue;
                        }
                        else
                        {
                            var objItems = clbListColumns.Items;
                            objItems.Add(objField.InternalName.ToString() + "; " + objField.FieldTypeKind.ToString());
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                MessageBox.Show(objException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region btnAddToList_Click

        private void btnAddToList_Click(object sender, EventArgs e)
        {
             ArrayList lstRemoveItems = new ArrayList();

             foreach (object objItemChecked in clbListColumns.CheckedItems)
             {
                 //objListItem[objItemChecked.ToString()] = Convert.ToString(p_dtExcelData.Rows[intRow][++intIndex]);
                 lsbSelectedColumns.Items.Add(objItemChecked.ToString());

                 lstRemoveItems.Add(objItemChecked);
             }

             foreach (var objItemRemoved in lstRemoveItems)
             {
                 clbListColumns.Items.RemoveAt(clbListColumns.Items.IndexOf(objItemRemoved));
             }
        }

        #endregion

        #region btnClearList_Click

        private void btnClearList_Click(object sender, EventArgs e)
        {
            btnFillColumns_Click(sender, e);
        }

        #endregion

        #region btnUpList_Click

        private void btnUpList_Click(object sender, EventArgs e)
        {
            UpClick(lsbSelectedColumns);
        }

        #endregion

        #region btnDownList_Click

        private void btnDownList_Click(object sender, EventArgs e)
        {
            DownClick(lsbSelectedColumns);
        }

        #endregion

        #region UpClick

        private void UpClick(ListBox p_lsbTarget)
        {
            if (p_lsbTarget.SelectedIndex > 0)
            {
                p_lsbTarget.Items.Insert(p_lsbTarget.SelectedIndex - 1, p_lsbTarget.SelectedItem.ToString());

                p_lsbTarget.SelectedIndex = (p_lsbTarget.SelectedIndex - 2);

                p_lsbTarget.Items.RemoveAt(p_lsbTarget.SelectedIndex + 2);
            }
        }

        #endregion

        #region DownClick

        private void DownClick(ListBox p_lsbTarget)
        {
            if ((p_lsbTarget.SelectedIndex != -1) && (p_lsbTarget.SelectedIndex < p_lsbTarget.Items.Count - 1))
            {
                p_lsbTarget.Items.Insert(p_lsbTarget.SelectedIndex + 2, p_lsbTarget.Text);

                p_lsbTarget.SelectedIndex = p_lsbTarget.SelectedIndex + 2;
                p_lsbTarget.Items.RemoveAt(p_lsbTarget.SelectedIndex - 2);
            }
        }

        #endregion
    }
}
