﻿namespace InsertExcelDataToSharePointList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofdExcel = new System.Windows.Forms.OpenFileDialog();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.lblChooseExcelFile = new System.Windows.Forms.Label();
            this.btnOpenDialog = new System.Windows.Forms.Button();
            this.btnClearFilePath = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.grbCrendentials = new System.Windows.Forms.GroupBox();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.lblDomain = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtSiteURL = new System.Windows.Forms.TextBox();
            this.lblSiteURL = new System.Windows.Forms.Label();
            this.lblSharePointList = new System.Windows.Forms.Label();
            this.txtSharePointList = new System.Windows.Forms.TextBox();
            this.clbListColumns = new System.Windows.Forms.CheckedListBox();
            this.lblListColumns = new System.Windows.Forms.Label();
            this.btnFillColumns = new System.Windows.Forms.Button();
            this.lsbSelectedColumns = new System.Windows.Forms.ListBox();
            this.btnAddToList = new System.Windows.Forms.Button();
            this.grbColumnsOrder = new System.Windows.Forms.GroupBox();
            this.btnUpList = new System.Windows.Forms.Button();
            this.lblColumnsOrder = new System.Windows.Forms.Label();
            this.btnClearList = new System.Windows.Forms.Button();
            this.btnDownList = new System.Windows.Forms.Button();
            this.grbCrendentials.SuspendLayout();
            this.grbColumnsOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofdExcel
            // 
            this.ofdExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.ofdExcel_FileOk);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(16, 600);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(218, 20);
            this.txtFilePath.TabIndex = 1;
            // 
            // lblChooseExcelFile
            // 
            this.lblChooseExcelFile.AutoSize = true;
            this.lblChooseExcelFile.Location = new System.Drawing.Point(16, 584);
            this.lblChooseExcelFile.Name = "lblChooseExcelFile";
            this.lblChooseExcelFile.Size = new System.Drawing.Size(103, 13);
            this.lblChooseExcelFile.TabIndex = 2;
            this.lblChooseExcelFile.Text = "Choose a Excel File:";
            // 
            // btnOpenDialog
            // 
            this.btnOpenDialog.Location = new System.Drawing.Point(242, 600);
            this.btnOpenDialog.Name = "btnOpenDialog";
            this.btnOpenDialog.Size = new System.Drawing.Size(75, 20);
            this.btnOpenDialog.TabIndex = 3;
            this.btnOpenDialog.Text = "Browse...";
            this.btnOpenDialog.UseVisualStyleBackColor = true;
            this.btnOpenDialog.Click += new System.EventHandler(this.btnOpenDialog_Click);
            // 
            // btnClearFilePath
            // 
            this.btnClearFilePath.Location = new System.Drawing.Point(325, 600);
            this.btnClearFilePath.Name = "btnClearFilePath";
            this.btnClearFilePath.Size = new System.Drawing.Size(75, 20);
            this.btnClearFilePath.TabIndex = 5;
            this.btnClearFilePath.Text = "Clear";
            this.btnClearFilePath.UseVisualStyleBackColor = true;
            this.btnClearFilePath.Click += new System.EventHandler(this.btnClearFilePath_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(7, 70);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(58, 13);
            this.lblUserName.TabIndex = 6;
            this.lblUserName.Text = "Username:";
            // 
            // grbCrendentials
            // 
            this.grbCrendentials.Controls.Add(this.txtDomain);
            this.grbCrendentials.Controls.Add(this.lblDomain);
            this.grbCrendentials.Controls.Add(this.txtPassword);
            this.grbCrendentials.Controls.Add(this.lblPassword);
            this.grbCrendentials.Controls.Add(this.txtUsername);
            this.grbCrendentials.Controls.Add(this.txtSiteURL);
            this.grbCrendentials.Controls.Add(this.lblSiteURL);
            this.grbCrendentials.Controls.Add(this.lblUserName);
            this.grbCrendentials.Location = new System.Drawing.Point(16, 12);
            this.grbCrendentials.Name = "grbCrendentials";
            this.grbCrendentials.Size = new System.Drawing.Size(384, 223);
            this.grbCrendentials.TabIndex = 7;
            this.grbCrendentials.TabStop = false;
            this.grbCrendentials.Text = "Credentials";
            // 
            // txtDomain
            // 
            this.txtDomain.Location = new System.Drawing.Point(10, 181);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.Size = new System.Drawing.Size(357, 20);
            this.txtDomain.TabIndex = 13;
            // 
            // lblDomain
            // 
            this.lblDomain.AutoSize = true;
            this.lblDomain.Location = new System.Drawing.Point(7, 165);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.Size = new System.Drawing.Size(46, 13);
            this.lblDomain.TabIndex = 12;
            this.lblDomain.Text = "Domain:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(10, 135);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(357, 20);
            this.txtPassword.TabIndex = 11;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(7, 118);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 10;
            this.lblPassword.Text = "Password:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(10, 86);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(357, 20);
            this.txtUsername.TabIndex = 9;
            // 
            // txtSiteURL
            // 
            this.txtSiteURL.Location = new System.Drawing.Point(10, 40);
            this.txtSiteURL.Name = "txtSiteURL";
            this.txtSiteURL.Size = new System.Drawing.Size(357, 20);
            this.txtSiteURL.TabIndex = 8;
            // 
            // lblSiteURL
            // 
            this.lblSiteURL.AutoSize = true;
            this.lblSiteURL.Location = new System.Drawing.Point(7, 24);
            this.lblSiteURL.Name = "lblSiteURL";
            this.lblSiteURL.Size = new System.Drawing.Size(198, 13);
            this.lblSiteURL.TabIndex = 7;
            this.lblSiteURL.Text = "SharePoint Site URL (with port number): ";
            // 
            // lblSharePointList
            // 
            this.lblSharePointList.AutoSize = true;
            this.lblSharePointList.Location = new System.Drawing.Point(13, 244);
            this.lblSharePointList.Name = "lblSharePointList";
            this.lblSharePointList.Size = new System.Drawing.Size(151, 13);
            this.lblSharePointList.TabIndex = 8;
            this.lblSharePointList.Text = "SharePoint List (display name):";
            // 
            // txtSharePointList
            // 
            this.txtSharePointList.Location = new System.Drawing.Point(16, 260);
            this.txtSharePointList.Name = "txtSharePointList";
            this.txtSharePointList.Size = new System.Drawing.Size(301, 20);
            this.txtSharePointList.TabIndex = 9;
            // 
            // clbListColumns
            // 
            this.clbListColumns.CheckOnClick = true;
            this.clbListColumns.FormattingEnabled = true;
            this.clbListColumns.Location = new System.Drawing.Point(6, 46);
            this.clbListColumns.Name = "clbListColumns";
            this.clbListColumns.Size = new System.Drawing.Size(361, 79);
            this.clbListColumns.TabIndex = 11;
            // 
            // lblListColumns
            // 
            this.lblListColumns.AutoSize = true;
            this.lblListColumns.Location = new System.Drawing.Point(6, 28);
            this.lblListColumns.Name = "lblListColumns";
            this.lblListColumns.Size = new System.Drawing.Size(240, 13);
            this.lblListColumns.TabIndex = 12;
            this.lblListColumns.Text = "Select List Columns (internal name and field type):";
            // 
            // btnFillColumns
            // 
            this.btnFillColumns.Location = new System.Drawing.Point(325, 260);
            this.btnFillColumns.Name = "btnFillColumns";
            this.btnFillColumns.Size = new System.Drawing.Size(75, 20);
            this.btnFillColumns.TabIndex = 13;
            this.btnFillColumns.Text = "Fill Columns";
            this.btnFillColumns.UseVisualStyleBackColor = true;
            this.btnFillColumns.Click += new System.EventHandler(this.btnFillColumns_Click);
            // 
            // lsbSelectedColumns
            // 
            this.lsbSelectedColumns.FormattingEnabled = true;
            this.lsbSelectedColumns.Location = new System.Drawing.Point(6, 173);
            this.lsbSelectedColumns.Name = "lsbSelectedColumns";
            this.lsbSelectedColumns.Size = new System.Drawing.Size(361, 69);
            this.lsbSelectedColumns.TabIndex = 14;
            // 
            // btnAddToList
            // 
            this.btnAddToList.Location = new System.Drawing.Point(210, 131);
            this.btnAddToList.Name = "btnAddToList";
            this.btnAddToList.Size = new System.Drawing.Size(75, 20);
            this.btnAddToList.TabIndex = 15;
            this.btnAddToList.Text = "Add to List";
            this.btnAddToList.UseVisualStyleBackColor = true;
            this.btnAddToList.Click += new System.EventHandler(this.btnAddToList_Click);
            // 
            // grbColumnsOrder
            // 
            this.grbColumnsOrder.Controls.Add(this.btnDownList);
            this.grbColumnsOrder.Controls.Add(this.btnUpList);
            this.grbColumnsOrder.Controls.Add(this.lblColumnsOrder);
            this.grbColumnsOrder.Controls.Add(this.btnClearList);
            this.grbColumnsOrder.Controls.Add(this.lblListColumns);
            this.grbColumnsOrder.Controls.Add(this.lsbSelectedColumns);
            this.grbColumnsOrder.Controls.Add(this.btnAddToList);
            this.grbColumnsOrder.Controls.Add(this.clbListColumns);
            this.grbColumnsOrder.Location = new System.Drawing.Point(16, 294);
            this.grbColumnsOrder.Name = "grbColumnsOrder";
            this.grbColumnsOrder.Size = new System.Drawing.Size(384, 283);
            this.grbColumnsOrder.TabIndex = 16;
            this.grbColumnsOrder.TabStop = false;
            this.grbColumnsOrder.Text = "Columns and Order";
            // 
            // btnUpList
            // 
            this.btnUpList.Location = new System.Drawing.Point(210, 248);
            this.btnUpList.Name = "btnUpList";
            this.btnUpList.Size = new System.Drawing.Size(75, 20);
            this.btnUpList.TabIndex = 17;
            this.btnUpList.Text = "Up";
            this.btnUpList.UseVisualStyleBackColor = true;
            this.btnUpList.Click += new System.EventHandler(this.btnUpList_Click);
            // 
            // lblColumnsOrder
            // 
            this.lblColumnsOrder.AutoSize = true;
            this.lblColumnsOrder.Location = new System.Drawing.Point(6, 157);
            this.lblColumnsOrder.Name = "lblColumnsOrder";
            this.lblColumnsOrder.Size = new System.Drawing.Size(112, 13);
            this.lblColumnsOrder.TabIndex = 17;
            this.lblColumnsOrder.Text = "Select Columns Order:";
            // 
            // btnClearList
            // 
            this.btnClearList.Location = new System.Drawing.Point(292, 131);
            this.btnClearList.Name = "btnClearList";
            this.btnClearList.Size = new System.Drawing.Size(75, 20);
            this.btnClearList.TabIndex = 16;
            this.btnClearList.Text = "Clear List";
            this.btnClearList.UseVisualStyleBackColor = true;
            this.btnClearList.Click += new System.EventHandler(this.btnClearList_Click);
            // 
            // btnDownList
            // 
            this.btnDownList.Location = new System.Drawing.Point(292, 248);
            this.btnDownList.Name = "btnDownList";
            this.btnDownList.Size = new System.Drawing.Size(75, 20);
            this.btnDownList.TabIndex = 18;
            this.btnDownList.Text = "Down";
            this.btnDownList.UseVisualStyleBackColor = true;
            this.btnDownList.Click += new System.EventHandler(this.btnDownList_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 653);
            this.Controls.Add(this.grbColumnsOrder);
            this.Controls.Add(this.btnFillColumns);
            this.Controls.Add(this.txtSharePointList);
            this.Controls.Add(this.lblSharePointList);
            this.Controls.Add(this.grbCrendentials);
            this.Controls.Add(this.btnClearFilePath);
            this.Controls.Add(this.btnOpenDialog);
            this.Controls.Add(this.lblChooseExcelFile);
            this.Controls.Add(this.txtFilePath);
            this.Name = "Form1";
            this.Text = "Insert Excel Data To SharePoint List";
            this.grbCrendentials.ResumeLayout(false);
            this.grbCrendentials.PerformLayout();
            this.grbColumnsOrder.ResumeLayout(false);
            this.grbColumnsOrder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog ofdExcel;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Label lblChooseExcelFile;
        private System.Windows.Forms.Button btnOpenDialog;
        private System.Windows.Forms.Button btnClearFilePath;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.GroupBox grbCrendentials;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.Label lblDomain;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtSiteURL;
        private System.Windows.Forms.Label lblSiteURL;
        private System.Windows.Forms.Label lblSharePointList;
        private System.Windows.Forms.TextBox txtSharePointList;
        private System.Windows.Forms.CheckedListBox clbListColumns;
        private System.Windows.Forms.Label lblListColumns;
        private System.Windows.Forms.Button btnFillColumns;
        private System.Windows.Forms.ListBox lsbSelectedColumns;
        private System.Windows.Forms.Button btnAddToList;
        private System.Windows.Forms.GroupBox grbColumnsOrder;
        private System.Windows.Forms.Label lblColumnsOrder;
        private System.Windows.Forms.Button btnClearList;
        private System.Windows.Forms.Button btnUpList;
        private System.Windows.Forms.Button btnDownList;
    }
}

